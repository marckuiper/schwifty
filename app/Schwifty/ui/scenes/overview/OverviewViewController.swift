//
//  OverviewViewController.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import UIKit

class OverviewViewController: UIViewController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)        
    }
    
    override func viewDidLoad() {
        print("Overview loaded")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Overview appeared")
    }
}
