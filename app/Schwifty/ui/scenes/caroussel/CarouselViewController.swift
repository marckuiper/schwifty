//
//  CarousselViewController.swift
//  Schwifty
//
//  Created by Marc Kuiper on 18/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import UIKit
import ReSwift

class CarouselViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var store: Store<AppState>!
    
    private var pages: [UIViewController] = []
    
    override func viewDidLoad() {
        
        let pageOne = storyboard!.instantiateViewController(withIdentifier: "pageOne") as! PageOneViewController
        pageOne.store = store // FIXME
        
        pages = [
            pageOne,
            storyboard!.instantiateViewController(withIdentifier: "pageTwo"),
            storyboard!.instantiateViewController(withIdentifier: "pageThree"),
        ]
        
        dataSource = self
        
        setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let index = pages.index(of: viewController) else { return nil }
        
        return index > 0
            ? pages[index - 1]
            : pages.last
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let index = pages.index(of: viewController) else { return nil }
        
        return index < pages.count - 1
            ? pages[index + 1]
            : pages.first
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard
            let page = viewControllers?.first,
            let index = pages.index(of: page)
            else { return 0 }
        
        return index
    }
}
