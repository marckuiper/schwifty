//
//  PageOneViewController.swift
//  Schwifty
//
//  Created by Marc Kuiper on 20/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import UIKit
import ReSwift

class PageOneViewController: UITableViewController, StoreSubscriber {
    
    var store: Store<AppState>!
    
    @IBOutlet weak var buttonsSwitch: UISwitch!
    @IBOutlet weak var segmentedSwitch: UISwitch!
    @IBOutlet weak var stepperSwitch: UISwitch!

    @IBOutlet weak var middleButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    @IBOutlet weak var buttonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var firstSegmented: UISegmentedControl!
    @IBOutlet weak var secondSegmented: UISegmentedControl!
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var stepperLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) {
            $0.select { $0.example.one }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        store.unsubscribe(self)
    }
    
    func newState(state: PageOneState) {
        
        buttonsSwitch.setOn(state.buttonsEnabled, animated: true)
        segmentedSwitch.setOn(state.segmentedEnabled, animated: true)
        stepperSwitch.setOn(state.stepperEnabled, animated: true)
        
        // Buttons
        
        middleButton.isEnabled = state.buttonsEnabled
        leftButton.isEnabled = state.buttonsEnabled
        rightButton.isEnabled = state.buttonsEnabled
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let self = self else { return }
            
            self.buttonLeadingConstraint.isActive = state.buttonPosition == .left
            self.buttonTrailingConstraint.isActive = state.buttonPosition == .right
            
            self.view.layoutIfNeeded()
        }
        
        // Segmented
        
        firstSegmented.isEnabled = state.segmentedEnabled
        secondSegmented.isEnabled = state.segmentedEnabled
        
        firstSegmented.selectedSegmentIndex = {
            switch state.segmentChoice {
            case .first: return 0
            case .second: return 1
            case .third: return 2
            }
        }()
        
        secondSegmented.selectedSegmentIndex = {
            switch state.segmentChoice {
            case .first: return 2
            case .second: return 1
            case .third: return 0
            }
        }()
        
        // Stepper
        
        stepper.isEnabled = state.stepperEnabled
        stepperLabel.isEnabled = state.stepperEnabled
        
        stepperLabel.text = String(state.stepperValue)
    }
    
    @IBAction func buttonsSwitchTapped(_ sender: UISwitch) {
        store.dispatch(ToggleButtonsAction(enabled: sender.isOn))
    }
    @IBAction func middleButtonTapped(_ sender: Any) {
        store.dispatch(SetButtonPosition(position: .middle))
    }
    @IBAction func leftButtonTapped(_ sender: Any) {
        store.dispatch(SetButtonPosition(position: .left))
    }
    @IBAction func rightButtonTapped(_ sender: Any) {
        store.dispatch(SetButtonPosition(position: .right))
    }
    
    @IBAction func segmentedSwitchTapped(_ sender: UISwitch) {
        store.dispatch(ToggleSegmentedAction(enabled: sender.isOn))
    }
    @IBAction func firstSegmentTapped(_ sender: UISegmentedControl) {
        store.dispatch(SetSegmentChoice(choice: {
            switch sender.selectedSegmentIndex {
            case 0: return .first
            case 1: return .second
            case 2: return .third
            default: return .first
            }
        }()))
    }
    @IBAction func secondSegmentTapped(_ sender: UISegmentedControl) {
        store.dispatch(SetSegmentChoice(choice: {
            switch sender.selectedSegmentIndex {
            case 0: return .third
            case 1: return .second
            case 2: return .first
            default: return .first
            }
        }()))
    }
    
    @IBAction func stepperSwitchTapped(_ sender: UISwitch) {
        store.dispatch(ToggleStepperAction(enabled: sender.isOn))
    }
    @IBAction func stepperTapped(_ sender: UIStepper) {
        store.dispatch(SetStepperValue(value: Int(sender.value)))
    }
}
