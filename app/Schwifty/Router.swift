//
//  Router.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import UIKit
import ReSwift

class Router: StoreSubscriber {
    
    let store: Store<AppState>
    let navigationController: UINavigationController
    
    init(store: Store<AppState>, navigationController: UINavigationController) {
        
        self.store = store
        self.navigationController = navigationController
        
        store.subscribe(self) { subscription in
            subscription.select { state in state.nav }
        }
    }
    
    deinit {
        store.unsubscribe(self)
    }
    
    func newState(state: String) {
        
        var vc: UIViewController?
        
        switch state {
        case "carousel":
            vc = UIStoryboard(name: "Carousel", bundle: nil).instantiateViewController(withIdentifier: "CarouselViewController")
            
            (vc as! CarouselViewController).store = store // FIXME
        case "overview":
            vc = UIStoryboard(name: "Overview", bundle: nil).instantiateViewController(withIdentifier: "OverviewViewController")
        case "detail":
            vc = UIStoryboard(name: "Detail", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController")
        default:
            print("No controller for route: " + state)
        }
        
        if let vc = vc {
//            navigationController.pushViewController(vc, animated: true)
            navigationController.show(vc, sender: self)
        }
    }
}
