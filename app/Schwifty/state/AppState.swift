//
//  AppState.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift

struct AppState: CodableState {
    
    var nav: String
    
    var example: ExampleState
}


//struct Habit: Codable {
//    let name: String
//}
//
//struct LoadHabitsAction: CodableAction {
//    let habits: [Habit]
//}
//
//struct SelectHabitAction: CodableAction {
//    let habit: Habit
//}
