//
//  ActionDecoder.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation

protocol ActionDecoder {
    func decode(type: String, action: String) throws -> CodableAction
}

enum DecodeError: Error {
    case unableToParseStringIntoData
    case unknownType(String)
}
