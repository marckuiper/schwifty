//
//  ActionEncoder.swift
//  Schwifty
//
//  Created by Marc Kuiper on 21/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift

protocol CodableState: StateType, Codable { }

protocol CodableAction: Action, Codable { }

class ActionEncoder {
    
    func encode(action: CodableAction) throws -> (String, String) {
        
        guard let name = parseStructName(action) else {
            throw DecodeError.failedToParseStructName
        }
        
        let data = try JSONEncoder().encode(AnyEncodable(action))
        
        guard let encoded = String(data: data, encoding: .utf8) else {
            throw DecodeError.invalidEncodedData
        }
        
        return (name, encoded)
    }
    
    func encode(state: CodableState) throws -> String {
        
        let data = try JSONEncoder().encode(AnyEncodable(state))
        
        guard let encoded = String(data: data, encoding: .utf8) else {
            throw DecodeError.invalidEncodedData
        }
        
        return encoded
    }
    
    private func parseStructName(_ test: Any) -> String? {
        let str = "\(test)"
        if let index = str.firstIndex(of: "(") {
            return String(str[..<index])
        }
        return nil
    }
    
    private struct AnyEncodable: Encodable {
        
        private let _encode: (Encoder) throws -> Void
        
        public init(_ wrapped: CodableAction) {
            _encode = wrapped.encode
        }
        
        public init(_ wrapped: CodableState) {
            _encode = wrapped.encode
        }
        
        func encode(to encoder: Encoder) throws {
            try _encode(encoder)
        }
    }
    
    enum DecodeError: Error {
        case failedToParseStructName
        case invalidEncodedData
    }
}
