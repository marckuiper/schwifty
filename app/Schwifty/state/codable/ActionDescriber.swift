//
//  ActionDescriptor.swift
//  Schwifty
//
//  Created by Marc Kuiper on 22/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation

protocol ActionDescriber {
    func getList() -> [ActionDescriptor]
}

struct ActionDescriptor: Codable {
    let name: String
    let props: [PropDescriptor]
}

struct PropDescriptor: Codable {
    let name: String
    let type: String
}

struct ActionListMessage: Codable {
    let type: String
    let actions: [ActionDescriptor]
}

extension ActionDescriber {
    func getMessage() throws -> String? {
        
        let msg = ActionListMessage(
            type: "ActionList",
            actions: getList()
        )
        
        return try String(data: JSONEncoder().encode(msg), encoding: .utf8)
    }
}
