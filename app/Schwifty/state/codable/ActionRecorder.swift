//
//  ActionRecorder.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift

class ActionRecorder<S: CodableState>: StoreSubscriber {
    
    private let server: StateServer
    private var store: Store<S>?
    private var actions: [CodableAction?]
    
    init(server: StateServer) {
        self.server = server
        self.actions = []
    }
    
    deinit {
        store?.unsubscribe(self)
    }
    
    func subscribe(to store: Store<S>) {
        self.store = store
        store.subscribe(self)
    }
    
    func getMiddleware() -> Middleware<S> {
        return { dispatch, getState in
            
            self.server.onPushAction { action in
                dispatch(action)
            }
            
            return { next in
                return { action in
                    
                    if action is ReSwiftInit {
                        self.actions.append(nil)
                        return next(action)
                    }
                    
                    if action is LoadStateAction {
                        return next(action)
                    }
                    
                    // TODO: handle non-codable actions
                    if let action = action as? CodableAction {
                        // Store action temporarily. It will be sent to the server
                        // along with the new state after it has been updated.
                        self.actions.append(action)
                    }
                    
                    return next(action)
                }
            }
        }
    }
    
    func newState(state: S) {
        
        guard actions.count != 0 else { return }
        
        // Sends the first action on the queue to the server along with
        // the new state.
        // TODO: figure out how to deal with asynchronous actions
        server.send(action: actions.removeFirst(), state: state)
    }
    
    enum RecordError: Error {
        case stateNotCodable
        case actionNotCodable
    }
}
