//
//  StateServer.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift
import Starscream

class StateServer {
    
    typealias PushActionCallback = (CodableAction) -> Void
    
    private var connected = false
    private var queuedMessages: [String] = []
    
    private var pushActionCallbacks: [PushActionCallback] = []
    
    private let ws: WebSocket
    private let encoder: ActionEncoder
    private let decoder: ActionDecoder
    
    init(ws: WebSocket, encoder: ActionEncoder, decoder: ActionDecoder, describer: ActionDescriber) {
        self.ws = ws
        self.encoder = encoder
        self.decoder = decoder
        
        ws.onConnect = {
            print("websocket is connected")
            self.connected = true
            
            do {
                if let listMessage = try describer.getMessage() {
                    ws.write(string: listMessage)
                }
            }
            catch {
                print(error)
            }
            
            self.queuedMessages.forEach { ws.write(string: $0) }
            self.queuedMessages.removeAll()
        }
        
        ws.onDisconnect = { (error: Error?) in
            print("websocket is disconnected: \(String(describing: error?.localizedDescription))")
            self.connected = false
        }
        
        ws.onText = { (text: String) in
            print("got some text: \(text)")
            
            guard let pipeIndex = text.firstIndex(of: "|") else { return }
            let nextIndex = text.index(pipeIndex, offsetBy: 1)
            
            let type = String(text[..<pipeIndex])
            let json = String(text[nextIndex...])
            
            do {
                let action = try decoder.decode(type: type, action: json)
                self.pushActionCallbacks.forEach { $0(action) }
            }
            catch {
                print(error)
            }
        }
    }
    
    func send(action: CodableAction?, state: CodableState) {
        
        do {
            var name: String? = nil
            var encodedAction: String? = nil
            let encodedState = try encoder.encode(state: state)

            if let action = action {
                let tuple = try encoder.encode(action: action)
                name = tuple.0
                encodedAction = tuple.1
            }
            
            let msg = "{"
                + "\"type\":\"RecordAction\","
                + "\"name\":\(name != nil ? "\"\(name!)\"" : "null"),"
                + "\"action\":\(encodedAction != nil ? encodedAction! : "null"),"
                + "\"state\":\(encodedState)"
                + "}"
            
            if !connected {
                queuedMessages.append(msg)
                return
            }
            
            ws.write(string: msg)
        }
        catch {
            print(error)
        }
    }
    
    func onPushAction(callback: @escaping PushActionCallback) {
        pushActionCallbacks.append(callback)
    }
}
