//
//  AppReducer.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift

struct LoadStateAction: CodableAction {
    let state: AppState
}

struct SetNavAction: CodableAction {
    let nav: String
}

func appReducer(action: Action, state: AppState?) -> AppState {
    
    switch action {
    
    case let action as LoadStateAction:
        return action.state
    
    default:
        return AppState(
            nav: navReducer(action: action, nav: state?.nav),
            example: exampleReducer(action: action, state: state?.example)
        )
    }
}

func navReducer(action: Action, nav: String?) -> String {
    
    switch action {
    case let set as SetNavAction:
        return set.nav
    default:
        return nav ?? "carousel"
    }
}
