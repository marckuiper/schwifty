//
//  ExampleState.swift
//  Schwifty
//
//  Created by Marc Kuiper on 18/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation

struct ExampleState: Codable {
    var page: CarouselPage
    var one: PageOneState
}

enum CarouselPage: String, Codable {
    case one
    case two
    case three
}

// MARK: - One

struct PageOneState: Codable {
    var buttonsEnabled: Bool
    var buttonPosition: ButtonPosition
    
    var segmentedEnabled: Bool
    var segmentChoice: SegmentChoice
    
    var stepperEnabled: Bool
    var stepperValue: Int
}

enum ButtonPosition: String, Codable {
    case left
    case middle
    case right
}

enum SegmentChoice: String, Codable {
    case first
    case second
    case third
}

