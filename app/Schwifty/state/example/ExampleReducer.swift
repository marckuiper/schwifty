//
//  ExampleReducer.swift
//  Schwifty
//
//  Created by Marc Kuiper on 18/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import ReSwift

struct ToggleButtonsAction: CodableAction {
    let enabled: Bool
}
struct SetButtonPosition: CodableAction {
    let position: ButtonPosition
}

struct ToggleSegmentedAction: CodableAction {
    let enabled: Bool
}
struct SetSegmentChoice: CodableAction {
    let choice: SegmentChoice
}

struct ToggleStepperAction: CodableAction {
    let enabled: Bool
}
struct SetStepperValue: CodableAction {
    let value: Int
}

func exampleReducer(action: Action, state: ExampleState?) -> ExampleState {
    
    var state = state ?? ExampleState(
        page: .one,
        one: PageOneState(
            buttonsEnabled: false,
            buttonPosition: .middle,
            
            segmentedEnabled: false,
            segmentChoice: .first,
            
            stepperEnabled: false,
            stepperValue: 0
        )
    )
    
    switch action {
        
    case let action as ToggleButtonsAction:
        state.one.buttonsEnabled = action.enabled
    case let action as SetButtonPosition:
        state.one.buttonPosition = action.position
        
    case let action as ToggleSegmentedAction:
        state.one.segmentedEnabled = action.enabled
    case let action as SetSegmentChoice:
        state.one.segmentChoice = action.choice
        
    case let action as ToggleStepperAction:
        state.one.stepperEnabled = action.enabled
    case let action as SetStepperValue:
        state.one.stepperValue = action.value
        
    default:
        break
    }
    
    return state
}
