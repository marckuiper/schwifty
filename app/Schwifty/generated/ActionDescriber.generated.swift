// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Foundation

class GeneratedActionDescriber: ActionDescriber {

    func getList() -> [ActionDescriptor] {
        return [
            action("LoadStateAction", [
                prop("state", "AppState")
            ]),
            action("SetButtonPosition", [
                prop("position", "ButtonPosition")
            ]),
            action("SetNavAction", [
                prop("nav", "String")
            ]),
            action("SetSegmentChoice", [
                prop("choice", "SegmentChoice")
            ]),
            action("SetStepperValue", [
                prop("value", "Int")
            ]),
            action("ToggleButtonsAction", [
                prop("enabled", "Bool")
            ]),
            action("ToggleSegmentedAction", [
                prop("enabled", "Bool")
            ]),
            action("ToggleStepperAction", [
                prop("enabled", "Bool")
            ])
        ]
    }

    private func action(_ name: String, _ props: [PropDescriptor]) -> ActionDescriptor {
        return ActionDescriptor(name: name, props: props)
    }

    private func prop(_ name: String, _ type: String) -> PropDescriptor {
        return PropDescriptor(name: name, type: type)
    }
}
