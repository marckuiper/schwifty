// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Foundation

class GeneratedActionDecoder: ActionDecoder {

    func decode(type: String, action: String) throws -> CodableAction {

        guard let data = action.data(using: .utf8) else {
            throw DecodeError.unableToParseStringIntoData
        }

        let decoder = JSONDecoder()
        var decoded: CodableAction

        switch type {

        case "LoadStateAction":
            decoded = try decoder.decode(LoadStateAction.self, from: data)

        case "SetButtonPosition":
            decoded = try decoder.decode(SetButtonPosition.self, from: data)

        case "SetNavAction":
            decoded = try decoder.decode(SetNavAction.self, from: data)

        case "SetSegmentChoice":
            decoded = try decoder.decode(SetSegmentChoice.self, from: data)

        case "SetStepperValue":
            decoded = try decoder.decode(SetStepperValue.self, from: data)

        case "ToggleButtonsAction":
            decoded = try decoder.decode(ToggleButtonsAction.self, from: data)

        case "ToggleSegmentedAction":
            decoded = try decoder.decode(ToggleSegmentedAction.self, from: data)

        case "ToggleStepperAction":
            decoded = try decoder.decode(ToggleStepperAction.self, from: data)

        default:
            throw DecodeError.unknownType(type)
        }

        return decoded
    }
}
