//
//  Module.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation

class Module {
    
    let app: DIApp
    
    init(app: DIApp) {
        self.app = app
    }
}
