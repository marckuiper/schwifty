//
//  StateModule.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation
import ReSwift
import Starscream

class StateModule: Module {
    
    private(set) lazy var store: Store<AppState> = {
        
        let store = Store<AppState>(
            reducer: appReducer,
            state: nil,
            middleware: [
                self.actionRecorder.getMiddleware()
            ]
        )
        
        self.actionRecorder.subscribe(to: store)
        
        return store
    }()
    
    private lazy var actionRecorder = ActionRecorder<AppState>(server: server)
    
    private(set) lazy var server = StateServer(
        ws: deviceSocket,
        encoder: ActionEncoder(),
        decoder: GeneratedActionDecoder(),
        describer: GeneratedActionDescriber()
    )
    
    private lazy var deviceSocket: WebSocket = {
        let socket = WebSocket(url: URL(string: "ws://localhost:9000/device")!)
        socket.connect()
        return socket
    }()
}
