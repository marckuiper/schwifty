//
//  DIApp.swift
//  Schwifty
//
//  Created by Marc Kuiper on 10/02/2019.
//  Copyright © 2019 Marc Kuiper. All rights reserved.
//

import Foundation
import UIKit

class DIApp {
    
    let app: UIApplication
    let nav: UINavigationController
    
    private(set) lazy var state = StateModule(app: self)
    
    init(app: UIApplication, nav: UINavigationController) {
        self.app = app
        self.nav = nav
    }
}

