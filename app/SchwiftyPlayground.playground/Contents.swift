import ReSwift

protocol CodableAction: Action, Codable { }

//class ActionDecoder {
//
//    let data: Data
//
//    init(data: Data) {
//        self.data = data
//    }
//
//    func decode<T: CodableAction>(_ type: T) throws -> T {
//        return try JSONDecoder().decode(type, from: data)
//    }
//}

struct AnyEncodable: Encodable {
    
    private let _encode: (Encoder) throws -> Void
//    public init<T: Encodable>(_ wrapped: T) {
//        _encode = wrapped.encode
//    }
    
    public init(_ wrapped: CodableAction) {
        _encode = wrapped.encode
    }
    
    func encode(to encoder: Encoder) throws {
        try _encode(encoder)
    }
}


struct Habit: Codable {
    let name: String
}

struct LoadHabitsAction: CodableAction {
    let habits: [Habit]
    
//    func decode(decoder: ActionDecoder<LoadHabitsAction>) throws -> CodableAction {
//        return try decoder.decode(LoadHabitsAction.self)
//    }
}

struct SelectHabitAction: CodableAction {
    let habit: Habit
}


func parseStructName(_ test: Any) -> String? {
    let str = "\(test)"
    if let index = str.firstIndex(of: "(") {
        return String(str[..<index])
    }
    return nil
}

struct EncodedAction {
    let type: String
    let action: String
}


let habit1 = Habit(name: "Dev")
let habit2 = Habit(name: "Test")
let habit3 = Habit(name: "Release")


var actions: [CodableAction] = [
    LoadHabitsAction(habits: [habit1, habit2, habit3]),
    SelectHabitAction(habit: habit1),
]

var typeMap: [String:Any] = [:]
var encoded: [EncodedAction] = []

try actions.forEach { action in
    
    let name = parseStructName(action)!
    
    typeMap[name] = action.self
    
    let wrapped = AnyEncodable(action)
    
    encoded.append(EncodedAction(
        type: name,
        action: try String(data: JSONEncoder().encode(wrapped), encoding: .utf8) ?? ""
    ))
}

//var types = [
//    "test1": LoadHabitsAction.self,
//    "test2": SelectHabitAction.self,
//]

enum MyError: Error {
    case runtimeError(String)
}

func decodeAction(type: String, action: String) throws -> CodableAction {
    
    guard let data = action.data(using: .utf8) else {
        throw MyError.runtimeError("")
    }
    
    var decoded: CodableAction
    
    switch type {
        
    case "LoadHabitsAction":
        decoded = try JSONDecoder().decode(LoadHabitsAction.self, from: data)
    case "SelectHabitAction":
        decoded = try JSONDecoder().decode(SelectHabitAction.self, from: data)
        
    default:
        throw MyError.runtimeError("")
    }
    
    return decoded
}

try encoded.forEach { action in
    
    print(action.type)
    print(action.action)
    
    print(try decodeAction(type: action.type, action: action.action))
    
//    if let data = action.action.data(using: .utf8) {
//
//        var decoded: CodableAction?
//
//        switch action.type {
//
//        case "LoadHabitsAction":
//            decoded = try JSONDecoder().decode(LoadHabitsAction.self, from: data)
//
//        case "SelectHabitAction":
//            decoded = try JSONDecoder().decode(SelectHabitAction.self, from: data)
//
//        default:
//            break
//        }
//
//        print(decoded ?? "")
        
//        let type = typeMap[action.type]
//        let decoded = try JSONDecoder().decode(type, from: data)
//        print(decoded)
//    }
}







//let habit = Habit(name: "Test")
//
//"\(habit.self)"
//
//
//let habitString = try String(data: JSONEncoder().encode(habit), encoding: .utf8)
//
//if let habitData = habitString?.data(using: .utf8) {
//    let decoded = try JSONDecoder().decode(Habit.self, from: habitData)
//    decoded.name
//}



