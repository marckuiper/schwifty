# Get Schwifty!

Just a little side project for playing around with making apps in Swift, and
related tasks. Made a commandline tool to control an app that uses ReSwift for
state management, like pushing actions and time traveling.

## Usage

(You're gonna need to be on OSX)

Open terminal on `[project_root]/server` and:

`npm install`

`npm run server`

`npm run client`

Then open `.xcworkspace` file in `[project_root]/app` and run app in simulator.
