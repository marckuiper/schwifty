
import * as express from 'express'
import {Application, Request} from 'express';
import expressWs = require('express-ws')
import WebSocket = require('ws')

export interface WebSocketApplication extends Application {
    ws: (path:string, handler:WebSocketHandler) => void
}

export interface WebSocketHandler {
    (ws:WebSocket, req:Request):void;
}

export function getWebSocketApplication():WebSocketApplication {

    const app = (express() as any) as WebSocketApplication;

    expressWs(app);

    return app;
}
