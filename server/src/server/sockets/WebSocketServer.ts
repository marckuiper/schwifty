
import * as express from 'express'
import WebSocket = require('ws')

type ConnectListener = (ws:WebSocket, req:express.Request) => any
type MessageListener<M,I> = (msg:M, id:I) => void
type ErrorListener = (e:Error) => void
type CloseListener<I> = (code:number, message:string, id:I) => void

// M = MessageType
// I = IdentityType
export default class WebSocketServer<M,I> {
    
    private connectListener:ConnectListener
    private messageListeners:MessageListener<M,I>[]
    private errorListeners:ErrorListener[]
    private closeListeners:CloseListener<I>[]
    
    constructor() {
        this.messageListeners = []
        this.errorListeners = []
        this.closeListeners = []
    }
    
    public getMiddleware() {
        return (this.connect as any).bind(this)
    }
    
    private connect(ws:WebSocket, req:express.Request) {
        
        const id = this.connectListener(ws, req)
        
        ws.on('message', (msgStr:string) => {
            try {
                const msg = JSON.parse(msgStr)
                this.messageListeners.forEach(listener => listener(msg, id))
            }
            catch (e) {
                console.log(e)
                console.log(msgStr)
            }
        });
        
        ws.on('error', (e:Error) => {
            this.errorListeners.forEach(listener => listener(e))
        });
        
        ws.on('close', (code, message) => {
            this.closeListeners.forEach(listener => listener(code, message, id))
        });
    }
    
    public onConnect(listener:ConnectListener) {
        this.connectListener = listener
    }
    
    public onMessage(listener:MessageListener<M,I>) {
        this.messageListeners.push(listener)
    }
    
    public onError(listener:ErrorListener) {
        this.errorListeners.push(listener)
    }
    
    public onClose(listener:CloseListener<I>) {
        this.closeListeners.push(listener)
    }
}
