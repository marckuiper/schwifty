import colors = require('colors/safe')
import WebSocketServer from './WebSocketServer';
import {DeviceConnections} from '../DeviceConnections';
import {WebSocketHandler} from '../WebSocketApplication';
import {CommanderMessage} from '../../messages/CommanderMessages';
import {CommanderConnections} from '../CommanderConnections';

export type CommanderId = string

export function createCommanderSocket(commanders:CommanderConnections, devices:DeviceConnections): WebSocketHandler {
    
    const server = new WebSocketServer<CommanderMessage, CommanderId>()
    
    server.onConnect(ws => {
        
        try {
            console.log('Connect commander')
            
            const commanderId = 'qwerty'
            
            commanders.addCommander(ws, commanderId)
            
            return commanderId
        }
        catch (e) {
            console.error(e)
        }
    })
    
    server.onClose((code, message, commanderId) => {
        console.log('Disconnect commander (status: %d, message: %s)', code, message)
        commanders.removeCommander(commanderId)
    })
    
    server.onMessage(msg => {
        
        try {
            console.log(colors.yellow('<<< ' + JSON.stringify(msg)))
            devices.send(msg)
        }
        catch (e) {
            console.error(e)
        }
    })
    
    return server.getMiddleware()
}
