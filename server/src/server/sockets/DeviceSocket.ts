import colors = require('colors/safe')
import WebSocketServer from './WebSocketServer';
import {DeviceConnections} from '../DeviceConnections';
import {WebSocketHandler} from '../WebSocketApplication';
import {DeviceMessage} from '../../messages/DeviceMessages';
import {CommanderConnections} from '../CommanderConnections';

export type DeviceId = string

export function createDeviceSocket(commanders:CommanderConnections, devices:DeviceConnections): WebSocketHandler {
    
    const server = new WebSocketServer<DeviceMessage,DeviceId>()
    
    server.onConnect(ws => {
        
        try {
            console.log('Connect device')

            //const deviceId = getParameterByName(CLIENT_ID_PARAM, ws.upgradeReq.url)
            const deviceId = 'asdf'
            
            if (!deviceId) {
                console.log('No deviceId given. Closing connection.')
                ws.close()
                return
            }
            
            devices.addDevice(ws, deviceId)
            
            return deviceId
        }
        catch (e) {
            console.error(e)
        }
    })
    
    server.onClose((code, message, deviceId) => {
        console.log('Disconnect device (status: %d, message: %s)', code, message)
        devices.removeDevice(deviceId)
    })
    
    server.onMessage(msg => {
        try {
            console.log(colors.cyan('>>> ' + JSON.stringify(msg)))
            commanders.send(msg)
        }
        catch (e) {
            console.error(e)
        }
    })
    
    return server.getMiddleware()
}
