
import WebSocket = require('ws')
import {CommanderMessage, PushActionMessage} from '../messages/CommanderMessages';
import {DeviceId} from './sockets/DeviceSocket';

export class DeviceConnections {
    
    private devices:Map<DeviceId, WebSocket> = new Map()
    private messages:CommanderMessage[] = []
    
    addDevice(socket:WebSocket, id:DeviceId) {
        this.devices.set(id, socket)
        
        this.messages.forEach(msg => {
            socket.send(this.convert(msg))
        })
    }
    
    removeDevice(id:DeviceId) {
        this.devices.delete(id)
    }
    
    send(msg:CommanderMessage) {
        
        this.messages.push(msg)
        
        const converted = this.convert(msg)
        
        this.devices.forEach(device => {
            device.send(converted)
        })
    }
    
    private convert(msg:CommanderMessage):string {
        
        if (msg.type == 'PushAction') {
            const push = msg as PushActionMessage
            return push.action.name + '|' + JSON.stringify(push.action.props)
        }
        
        throw 'Must convert message when sending it'
    }
}
