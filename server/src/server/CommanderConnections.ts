import WebSocket = require('ws')
import {DeviceMessage} from '../messages/DeviceMessages';
import {CommanderId} from './sockets/CommanderSocket';

export class CommanderConnections {
    
    private commanders:Map<CommanderId, WebSocket> = new Map()
    private messages:DeviceMessage[] = []
    
    addCommander(socket:WebSocket, id:CommanderId) {
        this.commanders.set(id, socket)
        
        this.messages.forEach(msg => {
            socket.send(JSON.stringify(msg))
        })
    }
    
    removeCommander(id:CommanderId) {
        this.commanders.delete(id)
    }
    
    send(msg:DeviceMessage) {
        
        this.messages.push(msg)
        
        const json = JSON.stringify(msg)
        
        this.commanders.forEach(commander => {
            commander.send(json)
        })
    }
}
