
export interface ActionDescriptor {
    name:string
    props:PropDescriptor[]
}

export interface PropDescriptor {
    name:string
    type:PropType
}

type PropType = 'object'|'number'|'string'



export interface Action {
    name:string
    props:{[name:string]:any}
}

export type State = object
