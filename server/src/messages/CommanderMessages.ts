import {Message} from './Message';
import {Action, State} from './Types';

export interface CommanderMessage extends Message { }

export interface PushActionMessage extends CommanderMessage {
    action:Action
}

export interface PushStateMessage extends CommanderMessage {
    state:State
}
