import {Message} from './Message';
import {Action, ActionDescriptor, State} from './Types';

export interface DeviceMessage extends Message { }

export interface ActionListMessage extends DeviceMessage {
    actions:ActionDescriptor[]
}

export interface RecordActionMessageRaw extends DeviceMessage {
    name:string
    action:string
    state:string
}

export interface RecordActionMessage extends DeviceMessage {
    name: string
    action:Action
    state:State
}
