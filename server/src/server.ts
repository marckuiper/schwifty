import {getWebSocketApplication} from './server/WebSocketApplication';
import {DeviceConnections} from './server/DeviceConnections';
import {createCommanderSocket} from './server/sockets/CommanderSocket';
import {createDeviceSocket} from './server/sockets/DeviceSocket';
import {CommanderConnections} from './server/CommanderConnections';

const port = 9000

const app = getWebSocketApplication()

app.get('/', function (req, res) {
    res.send('Hello!')
})

const commanders = new CommanderConnections()
const devices = new DeviceConnections()

app.ws('/commander', createCommanderSocket(commanders, devices))
app.ws('/device', createDeviceSocket(commanders, devices))

app.listen(port, () => {
    console.log(`Listening on port ${port}!`)
})
