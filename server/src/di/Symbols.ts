
export const Symbols = {
    
    ReadlineInterfaceFactory: Symbol('Factory<Interface>'),
    
    ReadStream: Symbol('ReadStream'),
    WriteStream: Symbol('WriteStream'),
    
    PropPromptFactory: Symbol('Factory<PropPrompt>')
    
}
