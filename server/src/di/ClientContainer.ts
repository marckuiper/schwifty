import 'reflect-metadata'
import {Container} from 'inversify'
import {CliModule} from './modules/CliModule';
import {ClientModule} from './modules/ClientModule';
import {StorageModule} from './modules/StorageModule';

const container = new Container()

container.load(
    ClientModule,
    CliModule,
    StorageModule
)

export { container }
