import {ContainerModule} from 'inversify'
import {ActionRepository} from '../../client/repo/ActionRepository';

export const StorageModule = new ContainerModule(bind => {
    
    bind<ActionRepository>(ActionRepository)
        .toSelf()
        .inSingletonScope()
    
})
