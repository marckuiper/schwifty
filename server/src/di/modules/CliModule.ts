import {ContainerModule, interfaces} from 'inversify'
import {createInterface, Interface} from "readline";
import {CommandLineInterface} from '../../client/CommandLineInterface';
import {Symbols} from '../Symbols';
import ReadStream = NodeJS.ReadStream;
import WriteStream = NodeJS.WriteStream;
import {ActionCommand} from '../../client/commands/ActionCommand';
import {ExitCommand} from '../../client/commands/ExitCommand';
import {CommandCommand} from '../../client/commands/CommandCommand';
import {LogCommand} from '../../client/commands/LogCommand';
import {StateCommand} from '../../client/commands/StateCommand';
import {ListCommand} from '../../client/commands/ListCommand';
import {HelpCommand} from '../../client/commands/HelpCommand';

export type Completer = (line:string) => [string[], string]

export type ReadlineInterfaceFactory = (completer: Completer) => Interface

export const CliModule = new ContainerModule(bind => {
    
    bind<CommandLineInterface>(CommandLineInterface)
        .toSelf()
        .inSingletonScope()
    
    bind<interfaces.Factory<Interface>>(Symbols.ReadlineInterfaceFactory)
        .toFactory<Interface>(context =>

            (completer:Completer) => createInterface({
                input: context.container.get<ReadStream>(Symbols.ReadStream),
                output: context.container.get<WriteStream>(Symbols.WriteStream),
                completer: completer
            })
        )
    
    bind<ReadStream>(Symbols.ReadStream).toConstantValue(process.stdin)
    bind<WriteStream>(Symbols.WriteStream).toConstantValue(process.stdout)
    
    bind<CommandCommand>(CommandCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<ListCommand>(ListCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<LogCommand>(LogCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<ActionCommand>(ActionCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<StateCommand>(StateCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<HelpCommand>(HelpCommand)
        .toSelf()
        .inSingletonScope()
    
    bind<ExitCommand>(ExitCommand)
        .toSelf()
        .inSingletonScope()
    
})
