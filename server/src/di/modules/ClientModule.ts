import {ContainerModule} from 'inversify'
import {CommanderClient} from '../../client/CommanderClient';
import {ActionSender} from '../../client/senders/ActionSender';

export const ClientModule = new ContainerModule(bind => {
    
    bind<string>('serverDomain')
        .toConstantValue('localhost:9000')
    
    bind<CommanderClient>(CommanderClient)
        .toSelf()
        .inSingletonScope()
    
    
    bind<ActionSender>(ActionSender)
        .toSelf()
    
})
