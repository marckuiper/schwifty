import {injectable, inject} from 'inversify'
import {CommanderClient} from '../CommanderClient';
import {ActionDescriptor, PropDescriptor} from '../../messages/Types';
import {PushActionMessage} from '../../messages/CommanderMessages';

@injectable()
export class ActionSender {
    
    private msg:PushActionMessage
    
    constructor(
        @inject(CommanderClient) private client:CommanderClient
    ) { }
    
    setAction(action:ActionDescriptor) {
        this.msg = {
            type: 'PushAction',
            action: {
                name: action.name,
                props: {}
            }
        }
    }
    
    setProp(prop:PropDescriptor, value:any) {
        if (!this.msg) return
        this.msg.action.props[prop.name] = this.parseType(prop.type, value)
    }
    
    send() {
        this.client.send(this.msg)
        this.msg = null
    }
    
    private parseType(type:string, value:any) {
        
        const is = (type:string, test:string) => {
            const regex = new RegExp(`^${test}\\?*$`)
            return regex.test(type.toLowerCase())
        }
        
        if (is(type, 'bool')) {
            return value == 'true'
        }
        if (is(type, 'int')) {
            return parseInt(value)
        }
        if (is(type, 'double') || is(type, 'float')) {
            return parseFloat(value)
        }
        
        return value
    }
}
