
import {injectable, inject} from 'inversify'
import WebSocket = require('ws')
import {Message} from '../messages/Message';

interface WebSocketEvents {
    message:(msg:any) => void
    error:(e:ErrorEvent) => void
    closed?:() => void
}

@injectable()
export default class WebSocketClient {
    
    @inject('serverDomain') private domain:string
    
    private ws: WebSocket
    private events: WebSocketEvents

    //constructor(@inject('serverDomain') private domain:string) { }
    
    public open(path:string, events:WebSocketEvents):Promise<void> {
        
        this.events = events

        return new Promise((resolve, reject) => {

            this.ws = new WebSocket('ws://' + this.domain + '/' + path);

            this.ws.onopen = () => {
                resolve();
            };
            
            this.ws.onerror = (event) => {
                reject(event.error);
            };

            this.ws.onmessage = (event) => {
                const msg = JSON.parse(event.data.toString())
                //const msg = event.data.toString()
                this.events.message(msg)
            }
            
            this.ws.onclose = () => {
                this.events.closed && this.events.closed()
            }
        })
    }

    public send(msg:Message) {
        this.ws.send(JSON.stringify(msg))
    }
}
