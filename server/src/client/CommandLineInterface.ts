import {inject, injectable} from 'inversify'
import {Interface, Key, clearLine, cursorTo, moveCursor} from 'readline'
import {Symbols} from '../di/Symbols';
import {ReadlineInterfaceFactory} from '../di/modules/CliModule';

@injectable()
export class CommandLineInterface {
    
    private rl: Interface
    private completions?: string[]
    
    constructor(
        @inject(Symbols.ReadlineInterfaceFactory) readlineFactory: ReadlineInterfaceFactory
    ) {
        this.rl = readlineFactory(this.completer.bind(this))
    }
    
    async prompt(prompt:string, completions:string[] = null):Promise<string> {

        this.rl.setPrompt(prompt)
        this.completions = completions

        return new Promise<string>((resolve, reject) => {
            this.rl.prompt()
            this.rl.once('line',input => {
                resolve(input)
            })
        })
    }
    
    write(data: string | Buffer, key?: Key) {
        this.rl.write(data, key)
    }
    
    writeLine(data: string | Buffer) {
        clearLine(process.stdout, 0)
        cursorTo(process.stdout, 0)
        moveCursor(process.stdout, 0, -1)
        this.rl.write(data + "\n")
    }
    
    private completer(line) {
        
        if (!this.completions) {
            return null
        }
        
        const hits = this.completions.filter((c) => c.startsWith(line))
        return [hits.length ? hits : this.completions, line]
    }
}

