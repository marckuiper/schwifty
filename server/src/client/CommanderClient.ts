import {injectable} from 'inversify'
import WebSocketClient from './WebSocketClient';
import {ActionListMessage, RecordActionMessage} from '../messages/DeviceMessages';
import {CommanderMessage} from '../messages/CommanderMessages';

type ActionListListener = (msg:ActionListMessage) => void
type ActionListener = (msg:RecordActionMessage) => void

@injectable()
export class CommanderClient extends WebSocketClient {
    
    private actionListListeners:ActionListListener[] = []
    private actionListeners:ActionListener[] = []
    
    connect():Promise<void> {
        return super.open('commander', {
            message: this.handleMessage.bind(this),
            error: e => { }
        })
    }
    
    private handleMessage(msg:CommanderMessage) {
        
        //console.log(msg)

        switch (msg.type) {

            case 'ActionList':
                this.actionListListeners.forEach((listener) => {
                    listener(msg as ActionListMessage)
                })
                break
            
            case 'RecordAction':
                this.actionListeners.forEach((listener) => {
                    listener(msg as RecordActionMessage)
                })
                break
        }
    }
    
    onActionList(listener:ActionListListener) {
        this.actionListListeners.push(listener)
    }
    
    onAction(listener:ActionListener) {
        this.actionListeners.push(listener)
    }
}
