import util = require('util')
import {injectable, inject} from 'inversify'
import {Command} from './CommandCommand';
import {ActionRepository} from '../repo/ActionRepository';
import {CommandLineInterface} from '../CommandLineInterface';

@injectable()
export class LogCommand implements Command {
    
    constructor(
        @inject(CommandLineInterface) private cli: CommandLineInterface,
        @inject(ActionRepository) private repo: ActionRepository
    ) { }
    
    async perform(args:string[]) {
        
        let records = this.repo.actionRecords
        let start = 0
        
        const amount = args.length != 0 ? parseInt(args[0]) : 20
        
        if (!isNaN(amount) && amount > 0) {
            start = Math.max(records.length - amount, 0)
            records = records.slice(-amount)
        }
        
        if (records.length == 0) {
            return
        }
        
        console.log('')
        records.forEach((record, index) => {
            
            const
                name = record.name ? record.name : 'Initial',
                action = record.action ? util.inspect(record.action) : ''
            
            console.log(`[${index+start}] ${name} ${action}`)
        })
        console.log('')
    }
}
