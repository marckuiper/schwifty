import {injectable, inject} from 'inversify'
import {Command} from './CommandCommand';
import {ActionRepository} from '../repo/ActionRepository';
import {CommandLineInterface} from '../CommandLineInterface';
import {ActionSender} from '../senders/ActionSender';
import {ActionDescriptor} from '../../messages/Types';

@injectable()
export class StateCommand implements Command {
    
    constructor(
        @inject(CommandLineInterface) private cli: CommandLineInterface,
        @inject(ActionRepository) private repo: ActionRepository,
        @inject(ActionSender) private sender: ActionSender
    ) { }
    
    async perform(args:string[]) {
        
        let index: number
            
        if (args.length != 0) {
            index = parseInt(args[0])
        }
        else {
            const prompt = await this.cli.prompt("  : ")
            index = parseInt(prompt)
        }
        
        if (isNaN(index)) {
            return
        }
        
        const records = this.repo.actionRecords
        
        if (records.length > index) {
            
            const action: ActionDescriptor = {
                name: "LoadStateAction",
                props: [
                    {
                        name: "state",
                        type: "object"
                    }
                ]
            }
            
            this.sender.setAction(action)
            
            this.sender.setProp(action.props[0], records[index].state)
            
            this.sender.send()
        }
    }
}
