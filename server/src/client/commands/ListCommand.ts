import {injectable, inject} from 'inversify'
import {ActionRepository} from '../repo/ActionRepository';
import {Command} from './CommandCommand';

@injectable()
export class ListCommand implements Command {
    
    constructor(
        @inject(ActionRepository) private repo: ActionRepository
    ) { }
    
    async perform(args:string[]) {
        
        const actions = this.repo.actionDescriptors
        
        const maxLength = actions.reduce((max, current) => {
            return Math.max(current.name.length, max)
        }, 0)
        
        console.log('')
        actions.forEach(action => {
            
            let spaces = " ".repeat(maxLength - action.name.length + 5)
            
            let props = action.props.map(prop =>
                `${prop.name}: ${prop.type}`
            )
            
            console.log(`${action.name}${spaces}(${props.join(', ')})`)
        })
        console.log('')
    }
}
