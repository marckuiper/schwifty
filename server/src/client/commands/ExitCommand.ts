import {injectable} from 'inversify'
import {Command} from './CommandCommand';

@injectable()
export class ExitCommand implements Command {
    
    async perform() {
        process.exit()
    }
}
