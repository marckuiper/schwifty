import {inject, injectable} from 'inversify'
import {CommandLineInterface} from '../CommandLineInterface';
import {ActionRepository} from '../repo/ActionRepository';

export interface Command {
    perform(args:string[]):Promise<void>
}

@injectable()
export class CommandCommand implements Command {
    
    private commands:{[name:string]:Command}
    
    constructor(
        @inject(CommandLineInterface) private cli:CommandLineInterface,
        @inject(ActionRepository) private repo:ActionRepository
    ) { }
    
    setCommands(commands:{[name:string]:Command}) {
        this.commands = commands
    }
    
    async perform() {
        
        let running = true
        
        while (running) {
            
            const
                keys = Object.keys(this.commands).concat(this.actionCommands()),
                input = await this.cli.prompt('> ', keys),
                words = input.split(' '),
                command = words.shift()
            
            if (keys.indexOf(command) != -1) {
                await this.commands[command].perform(words)
            }
        }
    }
    
    private actionCommands(): string[] {
        return this.repo.actionDescriptors.map(action =>
            'action ' + action.name
        )
    }
}
