import {injectable, inject} from 'inversify'
import {CommandLineInterface} from '../CommandLineInterface';
import {ActionSender} from '../senders/ActionSender';
import {ActionRepository} from '../repo/ActionRepository';
import {Command} from './CommandCommand';
import {ActionDescriptor, PropDescriptor} from '../../messages/Types';

@injectable()
export class ActionCommand implements Command {
    
    constructor(
        @inject(CommandLineInterface) private cli: CommandLineInterface,
        @inject(ActionRepository) private repo: ActionRepository,
        @inject(ActionSender) private sender: ActionSender
    ) { }
    
    async perform(args:string[]) {
        
        const action = await this.promptAction(args)
        
        if (!action) {
            return
        }
        
        this.sender.setAction(action)
        
        for (let [index, prop] of action.props.entries()) {
            
            let value = (args.length > index + 1)
                ? args[index + 1]
                : await this.promptProp(prop)
            
            this.sender.setProp(prop, value)
        }
        
        this.sender.send()
    }

    private async promptAction(args:string[]):Promise<ActionDescriptor> {
        
        const
            actions = this.repo.actionDescriptors,
            completions = actions.map(action => action.name)
        
        let name = (args.length > 0)
            ? args[0]
            : await this.cli.prompt(': ', completions)
        
        return actions.find(action => action.name == name)
    }

    private async promptProp(prop:PropDescriptor):Promise<string> {
        return await this.cli.prompt('   ' + prop.name + ': ')
    }
}
