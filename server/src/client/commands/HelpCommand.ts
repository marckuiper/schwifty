import {injectable, inject} from 'inversify'
import {Command} from './CommandCommand';

@injectable()
export class HelpCommand implements Command {

    private commands:{[name:string]:string}
    
    setCommands(commands:{[name:string]:string}) {
        return this.commands = commands
    }
    
    async perform() {

        const maxLength = Object.keys(this.commands).reduce((max, current) => {
            return Math.max(current.length, max)
        }, 0)

        console.log('')
        for (let command in this.commands) {
            const spaces = " ".repeat(maxLength - command.length + 5)
            console.log(command + spaces + this.commands[command])
        }
        console.log('')
    }
}
