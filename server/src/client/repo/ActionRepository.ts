import {injectable} from 'inversify'
import {Action, ActionDescriptor, State} from '../../messages/Types';

export interface ActionRecord {
    name:String
    action:Action
    state:State
}

@injectable()
export class ActionRepository {
    
    private _actionDescriptors:ActionDescriptor[] = []
    private _actionRecords:ActionRecord[] = []
    
    set actionDescriptors(actions:ActionDescriptor[]) {
        this._actionDescriptors = actions
    }
    
    get actionDescriptors():ActionDescriptor[] {
        return this._actionDescriptors
    }
    
    get actionRecords():ActionRecord[] {
        return this._actionRecords
    }
    
    addActionRecord(record: ActionRecord) {
        this._actionRecords.push(record)
    }
}
