import {container} from './di/ClientContainer';
import {CommanderClient} from './client/CommanderClient';
import {ActionCommand} from './client/commands/ActionCommand';
import {ExitCommand} from './client/commands/ExitCommand';
import {ActionRecord, ActionRepository} from './client/repo/ActionRepository';
import {CommandCommand} from './client/commands/CommandCommand';
import {LogCommand} from './client/commands/LogCommand';
import {StateCommand} from './client/commands/StateCommand';
import {ListCommand} from './client/commands/ListCommand';
import {HelpCommand} from './client/commands/HelpCommand';

const client = container.get<CommanderClient>(CommanderClient)

const command = container.get<CommandCommand>(CommandCommand)
const helpCommand = container.get<HelpCommand>(HelpCommand)

const actionRepository = container.get<ActionRepository>(ActionRepository)

client.onActionList(msg => {
    actionRepository.actionDescriptors = msg.actions
})

client.onAction(msg => {
    
    const record: ActionRecord = {
        name: msg.name,
        action: msg.action,
        state: msg.state,
    }
    
    actionRepository.addActionRecord(record)
})

command.setCommands({
    list: container.get<ListCommand>(ListCommand),
    log: container.get<LogCommand>(LogCommand),
    action: container.get<ActionCommand>(ActionCommand),
    state: container.get<StateCommand>(StateCommand),
    help: helpCommand,
    exit: container.get<ExitCommand>(ExitCommand),
})

helpCommand.setCommands({
    list: 'Display list of available actions with their props',
    log: 'Display list of dispatched actions, last 20 by default',
    action: 'Dispatch action to app',
    state: 'Override app state with previous state. See log for index number',
    help: 'Hello',
    exit: 'Bye',
})

async function main() {

    console.log('Connecting to server...')

    try {
        await client.connect()
    }
    catch {
        console.log('Failed to connect')
        process.exit()
    }
    
    console.log('Connected!')
    console.log('')
    
    await command.perform()
}

(async function() {
    await main()
})()
